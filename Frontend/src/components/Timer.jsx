import React, { useState } from "react";
import { useTimer } from "react-timer-hook";

function Timer({ expiryTimestamp, passwordToShow }) {
  const { seconds, minutes, restart, pause, start } = useTimer({
    expiryTimestamp,
    onExpire: () => {
      let now = new Date();
      now.setSeconds(now.getSeconds());
      restart(now);
      pause();
    },
  });

  const [password, setPassword] = useState(passwordToShow);

  if (password !== passwordToShow) {
    let now = new Date();
    now.setSeconds(now.getSeconds() + 30);
    restart(now);
    setPassword(passwordToShow);
  }

  return (
    <div className="flex flex-col justify-content items-center text-[#93e4c1]">
      {passwordToShow && <h1 className="text-3xl">{passwordToShow}</h1>}
      {!passwordToShow && <h1 className="text-3xl">No password yet</h1>}
      <div className="text-8xl">
        {seconds >= 10 && (
          <>
            <span>{minutes}</span>:<span>{seconds}</span>
          </>
        )}
        {seconds < 10 && (
          <>
            <span>{minutes}</span>:0<span>{seconds}</span>
          </>
        )}
      </div>
    </div>
  );
}

export default Timer;
