import React, { useState } from "react";
import axios from "axios";
import Timer from "./Timer";

function LoginForm() {
  const [user, setUser] = useState({});
  const [passwordToShow, setPasswordToShow] = useState("");
  const [time, setTime] = useState(new Date());

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setUser((values) => ({ ...values, [name]: value }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (user.id) {
      axios
        .post("https://localhost:7108/api/Totp/LoginUser", null, {
          params: {
            userId: user.id,
            password: user.password,
          },
        })
        .then((response) => {
          console.log(response.data.value);
          alert(response.data.value);
        })
        .catch((err) => {
          console.log(err);
          alert(err.message);
        });
    } else {
      alert("userId not defined");
    }
  };

  function getPassword() {
    if (user.id) {
      axios
        .get("https://localhost:7108/api/Totp/GetPassword", {
          params: {
            id: user.id,
          },
        })
        .then((response) => {
          console.log(response.data.value);
          if (!isNaN(response.data.value)) {
            setPasswordToShow(response.data.value);
            const now = new Date();
            now.setSeconds(now.getSeconds() + 30);
            setTime(new Date(now));
          } else {
            alert(response.data.value);
          }
        })
        .catch((err) => {
          console.log(err);
          alert(err.message);
        });
    } else {
      alert("userId not defined");
    }
  }

  return (
    <div className="flex flex-col justify-center items-center mt-3">
      <h1 className="text-3xl font-bold text-[#93e4c1]">
        Time-based one-time password
      </h1>
      <div className="flex flex-row justify-center items-center w-fit mx-auto space-x-[150px] mt-[150px]">
        <form action="post" className="flex flex-col space-y-2 ">
          <div className="flex flex-col space-y-2 w-fitmx-auto items-center">
            <input
              type="text"
              name="id"
              value={user.id || ""}
              onChange={handleChange}
              placeholder="userId"
              className="loginInput w-[100%] font-bold"
            />

            <input
              type="text"
              name="password"
              value={user.password || ""}
              onChange={handleChange}
              placeholder="password"
              className="loginInput w-[100%] font-bold"
            />
            <div className="flex flex-row space-x-2 w-fitmx-auto items-center">
              <button
                onClick={handleSubmit}
                type="submit"
                className="bg-[#93e4c1] py-5 px-10 rounded-md text-black font-bold text-lg"
              >
                Login
              </button>
              <button
                type="button"
                onClick={getPassword}
                className="bg-[#93e4c1] py-5 px-10 rounded-md text-black font-bold text-lg"
              >
                Get password
              </button>
            </div>
          </div>
        </form>
        <Timer
          expiryTimestamp={time}
          userId={user.id}
          passwordToShow={passwordToShow}
        />
      </div>
    </div>
  );
}

export default LoginForm;
