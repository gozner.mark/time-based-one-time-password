﻿using Microsoft.AspNetCore.Mvc;
using TimeBasedOneTimePasswordAPI.Models;
using TimeBasedOneTimePasswordAPI.Data;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using BCrypt.Net;

namespace TimeBasedOneTimePasswordAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TotpController : ControllerBase
    {
        private readonly TimeBasedOneTimePasswordAPIContext _context;

        public TotpController(TimeBasedOneTimePasswordAPIContext context)
        {
            _context = context;
        }

        // Create or login with user
        [HttpPost]
        public JsonResult LoginUser(long userId, int password)
        {
            var userInDb = _context.Users.Find(userId);

            if (userId <= 0)
            {
                return new JsonResult(NotFound());
            }

            if (userInDb == null)
            {
                // User creation
                User user = new()
                {
                    Id = userId
                };
                _context.Users.Add(user);

                _context.SaveChanges();
                return new JsonResult(Ok("User created!"));
            }

            // Check if at least 30 seconds has elapsed since the last password generation
            DateTime now = DateTime.UtcNow;
            if (userInDb.LastModified != null && 
                (now - DateTime.Parse(userInDb.LastModified)).TotalSeconds > 30)
            {
                Response.StatusCode = StatusCodes.Status401Unauthorized;
                return new JsonResult(Unauthorized($"Wrong credetials, your password has expired."));
            }

            // Password check
            if (userInDb.Password == null || !BCrypt.Net.BCrypt.Verify(password.ToString(), userInDb.Password))
            {
                Response.StatusCode = StatusCodes.Status401Unauthorized;
                return new JsonResult(Unauthorized($"Wrong credetials."));
            }

            return new JsonResult(Ok("Successful Login."));
        }

        // Get password for user
        [HttpGet]
        public JsonResult GetPassword(long id)
        {
            var userInDb = _context.Users.Find(id);

            if (userInDb == null)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
                return new JsonResult(NotFound());
            }

            DateTime now = DateTime.UtcNow;

            // Update user password if needed
            // Check if at least 30 seconds has elapsed since the last password generation
            // or the user has no password yet
            if (userInDb.LastModified == null 
                || userInDb.Password == null 
                || (now - DateTime.Parse(userInDb.LastModified)).TotalSeconds > 30)
            {
                int passwordToShow;
                (passwordToShow, userInDb.Password) = GeneratePassword();
                userInDb.LastModified = DateTime.UtcNow.ToString();
                _context.Users.Update(userInDb);
                _context.SaveChanges();

                return new JsonResult(Ok(passwordToShow));
            }

            return new JsonResult(Ok($"For user with id of {userInDb.Id} has elapsed " +
                        $"{((now - DateTime.Parse(userInDb.LastModified)).TotalSeconds)} " +
                        "since the last password generation."));
        }

        // Get all users
        [HttpGet]
        public JsonResult GetAllUsers()
        {
            var result = _context.Users.ToList();

            if (result.Count == 0)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
                return new JsonResult(NotFound());
            }

            List<string> users = new();
            DateTime now = DateTime.UtcNow;
            foreach (User u in result)
            {
                if(u.LastModified != null)
                {
                    users.Add($"For user with id of {u.Id} has elapsed " +
                        $"{((now - DateTime.Parse(u.LastModified)).TotalSeconds)} " +
                        "since the last password generation.");
                } else
                {
                    users.Add($"User with {u.Id} id has " +
                        $"no password yet.");
                }
                
            }

            return new JsonResult(Ok(users));
        }

        // password generation
        private static (int, string) GeneratePassword()
        {
            Random random = new();
            int password = random.Next(100000, 1000000);
            string salt = BCrypt.Net.BCrypt.GenerateSalt();
            string hashedPassword = BCrypt.Net.BCrypt.HashPassword(password.ToString(), salt);

            return (password, hashedPassword);
        }
    }
}
