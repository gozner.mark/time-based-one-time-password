﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using TimeBasedOneTimePasswordAPI.Models;

namespace TimeBasedOneTimePasswordAPI.Data
{
    public partial class TimeBasedOneTimePasswordAPIContext : DbContext
    {
        public TimeBasedOneTimePasswordAPIContext()
        {
        }

        public TimeBasedOneTimePasswordAPIContext(DbContextOptions<TimeBasedOneTimePasswordAPIContext> options)
            : base(options)
        {
        }

        public virtual DbSet<User> Users { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
