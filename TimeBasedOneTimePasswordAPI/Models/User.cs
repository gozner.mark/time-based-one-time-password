﻿using System;
using System.Collections.Generic;

namespace TimeBasedOneTimePasswordAPI.Models
{
    public partial class User
    {
        public long Id { get; set; }
        public string? Password { get; set; }
        public string? LastModified { get; set; }
    }
}
